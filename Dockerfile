FROM nginx:1.22-alpine
COPY . /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf
# Replace code placeholder with IMAGE_TAG 
ARG IMAGE_TAG=""
RUN sed -i "s/_VERSION_/${IMAGE_TAG}/" /usr/share/nginx/html/index.html